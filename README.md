# Casayuda

## API REST

### Dependencias para el servidor de desarrollo, aparte de Django

```
python3 -m pip install django-cors-headers
python3 -m pip install pymysql
python3 -m pip install bcrypt
```

## Claves de Inicio de los Usuarios Normales

|  Email | Contraseña  |
|---|---|
| pepedepura@fpcoruna.afundacion.org  | 1234  |
| manuelmartinez@fpcoruna.afundacion.org  | 1234  |
| andrearodriguez@fpcoruna.afundacion.org  | 1234  |
| carloscanosa@fpcoruna.afundacion.org  | 1234  |
| albertocampos@fpcoruna.afundacion.org  | 1234  |
| pedrosantos@fpcoruna.afundacion.org  | 1234  |


## Claves de Inicio de los Usuarios Trabajadores

|  Email | Contraseña  |
|---|---|
| juliaalvarez@fpcoruna.afundacion.org  | 1234  |
| martincastro@fpcoruna.afundacion.org  | 1234  |
| alvarocanosa@fpcoruna.afundacion.org  | 1234  |
| peperojo@fpcoruna.afundacion.org  | 1234  |
| juliancalvo@fpcoruna.afundacion.org  | 1234  |
| rodrigoferreiro@fpcoruna.afundacion.org  | 1234  |
| juanvazquez@fpcoruna.afundacion.org  | 1234  |
| pedrocampos@fpcoruna.afundacion.org  | 1234  |
| angelmartinez@fpcoruna.afundacion.org  | 1234  |
| joseperez@fpcoruna.afundacion.org  | 1234  |
