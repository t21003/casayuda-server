from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
import bcrypt
import json
import secrets
from django.http import HttpResponse
from .models import Tuser


#REGISTRO
#Para registrar un usuario, primero se identifica si es un trabajador o no.
#Para cada uno, se comprueban una serie de posibles errores.
#Si todos los parámetros son correctos se registrará al usuario/trabajador 
@csrf_exempt
def register(request):
    json_body = json.loads(request.body)
    # Se hace un try catch del objeto worker. Si no existe se entiende que es un usuario normal.
    # Si existe se comprueban los demás datos.
    try:
        worker_body = json_body['worker']
        try:
            name_body = json_body['name']
            surname_body = json_body['surname']
            email_body = json_body['email']
            password_body = json_body['password']
            precio_body = json_body['worker']['price_per_hour']
            latitude_body = json_body['worker']['latitude']
            longitude_body = json_body['worker']['longitude']

            # Comprobamos que existan trabajos en el JSON y que estén permitidos
            jobs_body = json_body['worker']['jobs']
            jobs_string = ''
            if len(jobs_body) == 0:
                return JsonResponse(status=400, data={})

            jobs_accepted = ['cleaner', 'cooker', 'plumber',
                             'electrician', 'babysitter', 'caregiver']
            for jobs_body_item in jobs_body:
                jobs_string = jobs_string + jobs_body_item + ','
                if not jobs_body_item in jobs_accepted:
                    return JsonResponse(status=400, data={})
            # LLegados aquí, todos los parámetros son correctos.
            # Es necesario comprobar que no haya nadie registrado con el mismo correo
            # Si el correo no está registrado, se registra al nuevo trabajador

	        #Se elimina la última coma del string que contiene todos los trabajos de los usuarios 	   
            jobs_string=jobs_string[:-1]

            try:
                Tuser.objects.get(email=email_body)
                return JsonResponse(status=409, data={})
            except Tuser.DoesNotExist:
                salt = bcrypt.gensalt()
                encrypted_pass = bcrypt.hashpw(
                    password_body.encode('utf-8'), salt).decode('utf-8')

                new_user = Tuser()
                new_user.name = name_body
                new_user.surname = surname_body
                new_user.email = email_body
                new_user.encrypted_password = encrypted_pass
                new_user.jobs = jobs_string
                new_user.price_per_hour = precio_body
                new_user.latitude = latitude_body
                new_user.longitude = longitude_body
                new_user.save()
                return JsonResponse(status=201, data={})
        except KeyError:
            return JsonResponse(status=400, data={})

    except KeyError:
        # Usuario normal
        # Se comprueba que se reciban todos los datos esperados
        # Se guardan todos los parámetros esperados en variables
        try:
            name_body = json_body['name']
            surname_body = json_body['surname']
            email_body = json_body['email']
            password_body = json_body['password']
        except KeyError:
            return JsonResponse(status=400, data={})

        # Se comprueba que no haya nadie registrado con el mismo mail
        # Si el mail existe en la BBDD, se devuelve el código 409; si NO existe se registra y se devuele un 201
        try:
            Tuser.objects.get(email=email_body)
            return JsonResponse(status=409, data={})
        except Tuser.DoesNotExist:
            salt = bcrypt.gensalt()
            encrypted_pass = bcrypt.hashpw(
                password_body.encode('utf-8'), salt).decode('utf-8')

            new_user = Tuser()
            new_user.name = name_body
            new_user.surname = surname_body
            new_user.email = email_body
            new_user.encrypted_password = encrypted_pass
            new_user.save()
            return JsonResponse(status=201, data={})

#LOGIN
#En el login se comprueba que los datos recibidos sean correctos y coincidan con la BBDD
#Si todo está correcto, el usuario iniciará sesión.
#En cambio, si no es así, recibirá un código de error
@csrf_exempt
def session(request):
    json_body = json.loads(request.body)

    # Comprobación de que se recibe un email y una contraseña
    try:
        email_body = json_body['email']
        password_body = json_body['password']
    except KeyError:
        return JsonResponse(status=400, data={})

    # Comprobación de que el mail que se recibe existe en la BBDD
    try:
        user_bbdd = Tuser.objects.get(email=email_body)
    except Tuser.DoesNotExist:
        return JsonResponse(status=404, data={})

    # Comprobación de que la contraseña es correcta
    # Si es correcta se loguea al usuario. Si no lo es, se le manda un error 401
    password_bbdd = user_bbdd.encrypted_password
    if bcrypt.checkpw(password_body.encode('utf-8'), password_bbdd.encode('utf-8')):

        id_user = user_bbdd.id
        name_user = user_bbdd.name

        # ¿Por qué latin-1? 
        # https://stackoverflow.com/questions/5552555/unicodedecodeerror-invalid-continuati>
        #token = secrets.token_bytes(16).decode('latin-1')
        token = secrets.token_hex(10) # Modified not because the 'bytes' version is wrong... But to ease the client app development
        user_bbdd.active_session_token = token
        user_bbdd.save()

        if (user_bbdd.jobs == None):
            is_worker = False
	    #La app cliente enviará al usuario a: workers
        else:
            is_worker = True
	    #La app cliente enviará al usuario a: workers/{id}/requests 

        data = {
            'user_id': user_bbdd.id,
            'name': user_bbdd.name,
            'is_worker': is_worker,
            'token': token
        }

        return JsonResponse(data, status=201)
    else:
        return JsonResponse(status=401, data={})
