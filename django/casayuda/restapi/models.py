# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class Tcomment(models.Model):
    author_user = models.ForeignKey('Tuser', models.DO_NOTHING, related_name='authored_comments')
    worker_user = models.ForeignKey('Tuser', models.DO_NOTHING, related_name='referring_comments')
    message = models.CharField(max_length=200, blank=True, null=True)
    rating = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'tComment'


class Trequest(models.Model):
    datetime = models.DateTimeField()
    message = models.CharField(max_length=200)
    requester_user = models.ForeignKey('Tuser', models.DO_NOTHING, related_name='authored_requests')
    worker_user = models.ForeignKey('Tuser', models.DO_NOTHING, related_name='worker_requests')
    is_accepted = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'tRequest'


class Tuser(models.Model):
    name = models.CharField(max_length=50)
    surname = models.CharField(max_length=100)
    email = models.CharField(unique=True, max_length=200)
    encrypted_password = models.CharField(max_length=100)
    jobs = models.CharField(max_length=200, blank=True, null=True)
    price_per_hour = models.IntegerField(blank=True, null=True)
    latitude = models.DecimalField(max_digits=8, decimal_places=6, blank=True, null=True)
    longitude = models.DecimalField(max_digits=9, decimal_places=6, blank=True, null=True)
    active_session_token = models.CharField(max_length=20, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'tUser'
