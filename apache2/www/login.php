<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Login</title>
	<!-- Llamamos a la función java Script -->
	<script type="text/javascript" src="static/login.js"></script>
	<!-- Llamamos a los estilos de css -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="static/css/Login_estilos.css" type="text/css">
</head>
<style>
	@import url('https://fonts.googleapis.com/css2?family=Abril+Fatface&family=Roboto:ital,wght@0,100;0,300;1,100;1,300&display=swap');
</style>
<body>
	<!-- La cabecera del codigo, donde aparece el logo, banner de Casa Ayuda -->
	<div class="header">
       <p class="logo">Casa Ayuda</p>
	   <div class="header-right">
            <ul>
            	<li class="registro"><a href="/register.php">Registrate</a></li>
            </ul>
        </div>
	</div>

	<!-- El cuerpo donde está el formulario de Login. -->
	<div class="cuerpo">
		<!-- La acción nos manda los parametros a do_login.php -->
		<form method="POST" action="do_login.php" id="login">
			<labe>Email</label><br>
			<!-- Aquí pasamos el email con el parametro email_p -->
			<input name="email_p" id="email_p" type="text"><br><br>
			<labe>Contraseña</label><br>
			<!-- Aqui pasamos la contraseña con el parametro pass_p -->
			<input name="pass_p" id="pass_p" type="password"><br>
			<input type="submit" value="Login">
		</form>
	</div>
	<!-- Aqui, tenemos la contenedor que nos va a mostrar si hay algún error en el login. -->
	<div class="Pie_Alertas">
	
	<?php 
		// Metemos en variables los parametro recibidos en la url si hay fallo en el login
		$pass_fail = $_GET['login_failed_password'];
		$email_fail = $_GET['login_failed_email'];
		$login_fail_unknown = $_GET['login_failed_unknown'];

		// Si entra dentro del if, pinta por pantalla que la contraseña es incorrecta
		if($pass_fail == TRUE){
			echo ('<h1>La Contraseña es incorrecta, Inténtelo de Nuevo.</h1>');
		}
		// Si entra dentro del if, pinta por pantalla el email es incorrecto
		if ($email_fail == TRUE){
			echo ('<h1>El E-MAIL introducido es incorrecto, Inténtelo de Nuevo.</h1>');
		}
		// Si entra dentro del if, pinta por pantalla que hubo algún error desconocido.
		if ($login_feil_unknown == TRUE){
			echo ('<h1>Cachis... Algo ha salido mal. Intentalo de nuevo más adelante. Disculpa Las molestias.</h1>');
		}
	?>
	</div>	
</body>

</html>
