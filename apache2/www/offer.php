<?php
    //Permite la conexión a la base de datos
    require __DIR__ . '/../php_util/db_connection.php';
    $mysqli = get_db_connection_or_die()
?>
<?php
    //Librería para enviar correos
    require_once __DIR__ . '/phpmailer/src/Exception.php';
    require_once __DIR__ . '/phpmailer/src/PHPMailer.php';
    require_once __DIR__ . '/phpmailer/src/SMTP.php';

    use PHPMailer\PHPMailer\PHPMailer;
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Oferta</title>
        <link rel="stylesheet" href="./static/css/Pantallas_estilos.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    </head>
    <style>
        @import url('https://fonts.googleapis.com/css2?family=Abril+Fatface&family=Roboto:ital,wght@0,100;0,300;1,100;1,300&display=swap');
        .oferta{
            text-align: center; 
            border: 1px solid black; 
            border-radius: 10px; 
            padding: 10px; 
            margin-left: 20%; 
            margin-right: 20%; 
            margin-top:3%;
        }
        .aceptar{
            border-radius: 10px; 
            padding: 10px; 
            color: black;
        }
        .aceptar:hover{
            font-weight: bold;
        }
        div.contenedor {
        margin: 10em auto;
        border: 1px solid black;
        border-radius: 10px;
        height: 200px;
        width: 600px;
        background-color: #e3e3e3;
        display: flex;
        justify-content: center;
        align-items: center;
        }
    </style>
    <body>
    <?php
        session_start();
        
        //Si un trabajador entra sin encontrarse logueado, saldrá la siguiente página de error
        if (empty($_SESSION['user_id'])){
            echo '<div class="header">';
            echo '<p class="logo">Casayuda</p>';
            echo '</div>';
            echo '<div class="contenedor">';
            echo '<p>No se encuentra logueado, para poder logearse pulse <a href="/login.php">aquí</a>.</p>';
            echo '</div>';
            die();
        }

        $idoferta=$_GET['id'];
        //Si no existe la variable 'id' mostrará una página de error
        if(!isset($idoferta)){
            http_response_code(404);
            echo '<h1>OOPS!</h1>';
            echo '<h1>Error 404</h1>';
            echo '<p>No se ha solicitado ninguna id</p>';
            die();
        }
        //Si el valor de 'id' es nulo mostrará una página de error
        if($idoferta == NULL){
            http_response_code(404);
            echo '<h1>OOPS!</h1>';
            echo '<h1>Error 404</h1>';
            echo '<p>La id introducida es nula</p>';
            die();
        }

        //Encabezado
        echo '<div class="header">';
                        echo '<p>Casa Ayuda</p>';
                        echo '<div class="header-right">';
                            echo '<ul>';
                            echo '<li>';
                                //Implementado el buscador (propuesta de Francisco Caneda)
                                echo '<form method="GET">';
                                    echo '<input id=buscador name="buscador" type="text" placeholder="Buscar..."></input>';
                                    echo '<button type="submit"><i class="fa fa-search"></i></button>';
                                echo '</form>';
                            echo '</li>';
                            echo '<li class="cerrarSesion"><a href="/worker.php">Lista Ofertas</a></li>';
                            echo '<li class="cerrarSesion"><a href="/do_logout.php">Cerrar Sesión</a></li>';
                            echo '</ul>';
                        echo '</div>';
                    echo '</div>';
        
        echo '<h1 style="text-align: center; margin-top: 50px;">Oferta '.$idoferta.'</h1>';
        //Consulta a la BBDD que mostrará los datos de la oferta
        $consulta= 'SELECT tUser.name, tRequest.datetime, tRequest.message FROM tRequest JOIN tUser ON tUser.id = tRequest.requester_user_id WHERE tRequest.id ='.$idoferta.'';
        $stmt= $mysqli->prepare($consulta);
        $stmt->execute();
        $resultado = $stmt->get_result();
        while($fila = mysqli_fetch_array($resultado)){
            echo '<div class="oferta">';
            echo '<h3>Datos de la oferta</h3>';
            echo '<p><b>Cliente:</b> '.$fila['name'].'</p>';
            echo '<p><b>Fecha y hora:</b> '.$fila['datetime'].'</p>';
            echo '<p><b>Descripción:</b> '.$fila['message'].'</p>';
            echo '<br>';
        }    
        echo '<form method="post">';
        echo '<input type="submit" name="boton" class="aceptar" value="Aceptar"/>';
        echo '</form>';
            
        //Al pulsar el botón aceptar se ejecuta la consulta para cambiar el estado de la oferta
        if(isset($_POST['boton'])) {
            $consultaAceptar='UPDATE tRequest SET is_accepted = true WHERE id = '.$idoferta.'';
            $stmt= $mysqli->prepare($consultaAceptar);
            $stmt->execute();
            echo '<p>Oferta aceptada</p>';
            
            //Consulta para acceder al correo del usuario que realizó la oferta
            $consultaCorreo='SELECT tUser.email FROM tUser JOIN tRequest ON tUser.id = tRequest.requester_user_id WHERE tRequest.id ='.$idoferta.' ';
            $stmt= $mysqli->prepare($consultaCorreo);
            $stmt->execute();
            $resultadoCorreo = $stmt->get_result();
            while($fila = mysqli_fetch_array($resultadoCorreo)){
                //Instanciamos la clase para poder enviar el correo
                $correo = new PHPMailer();
                $correo->IsSMTP();

                try{
                    //Configuración del servidor de correo
                    $correo->From = "t2notifications_no_reply@fpcoruna.afundacion.org";
                    $correo->SMTPAuth = true;
                    $correo->SMTPSecure = 'tls';
                    $correo->Host = "smtp.gmail.com";
                    $correo->Port = 587;
                    $correo->Username = "t2notifications_no_reply@fpcoruna.afundacion.org";
                    $correo->Password = 'Password';
                    //Destinatario
                    $correo->AddAddress($fila['email']);
                    $correo->Subject = "Oferta Casayuda";
                    $correo->MsgHTML("Le informamos que la oferta que usted ha publicado en Casayuda ha sido aceptada por uno de nuestros trabajadores
                    . Dicho trabajador se acercará por su casa a la hora acordada.");

                    if($correo -> Send()){
                        echo "<p>Se informará al usuario mediante email que la oferta ha sido aceptada</p>";
                    } else {
                        echo "<p>ERROR. No se podido en enviar un email al usuario</p>";
                        echo $correo -> ErrorInfo;
                    }
                } catch (Exception $e){
                    echo $e->errorMessage();
                } catch (\Exception $e){
                    echo $e->getMessage();
                }
            }
        }
        echo '</div>';
        $stmt->close();
        mysqli_close($mysqli);
        ?>
    </body>
</html>