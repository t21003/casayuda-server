<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="./static/css/workerDetail.css" type="text/css">
    <style>

        @import url('https://fonts.googleapis.com/css2?family=Abril+Fatface&family=Roboto:ital,wght@0,100;0,300;1,100;1,300&display=swap');

    </style>
</head>
<body>
<?php
    session_start();

        $idworker=$_GET['id'];
        //Si no existe la variable 'id' mostrará una página de error
        if(!isset($idworker)){
        ?>
	<div class="header">
            <p class="logo"><a class="logomain" href="/main.php">Casayuda</a></p>
	    <div class="header-right">
                <ul>
                    <!-- Este sería el botón para poder cerrar la sesión el cuál te lleva a la página do_logout.php -->
                    <li class="cerrarSesion"><a href="/do_logout.php">Cerrar Sesión</a></li>
                </ul>
            </div>
        </div>
        <div class="contenedorLogin">
            <p>No se ha enviado niguna id , para volver a main pulsa <a href='/main.php'>aquí</a>.</p>
        </div>
        <?php
	}
        //Si el valor de 'id' es nulo mostrará una página de error
        else if($idworker == NULL){
        ?>
	<div class="header">
            <p class="logo"><a class="logomain" href="/main.php">Casayuda</a></p>
	    <div class="header-right">
                <ul>
                    <!-- Este sería el botón para poder cerrar la sesión el cuál te lleva a la página do_logout.php -->
                    <li class="cerrarSesion"><a href="/do_logout.php">Cerrar Sesión</a></li>
                </ul>
            </div>

	</div>
        <div class="contenedorLogin">
            <p>El valor de id está vacio , para volver a main pulsa <a href='/main.php'>aquí</a>.</p>
        </div>
        <?php
        }
    /*
        Compruebo si el usuario está logueado. Si no lo esta lo envió a una página de error con un enlace a login.php que se pueda loguear.
        Y si el usuario está logueado lo llevo a la pantalla de detalle del trabajador.
    */

        else if (empty($_SESSION['user_id'])) {
        ?>
        <div class="header">
            <p class="logo"><a class="logomain" href="/main.php">Casayuda</a></p>

        </div>
        <div class="contenedorLogin">
            <p>No estás logueado, para poder logearse pulse <a href='/login.php'>aquí</a>.</p>
        </div>

       <?php
       } else {
       ?>
        <div class="header">
            <p class="logo"><a class="logomain" href="/main.php">Casayuda</a></p>
            <div class="header-right">
                <ul>
                    <!-- Este sería el botón para poder cerrar la sesión el cuál te lleva a la página do_logout.php -->
                    <li class="cerrarSesion"><a href="/do_logout.php">Cerrar Sesión</a></li>
                </ul>
            </div>
        </div>
	<?php

        // Indicamos que se require el siguiente fichero
        require __DIR__ . '/../php_util/db_connection.php';
        // Conectamos con la base de datos
        $mysqli = get_db_connection_or_die();
        //Preparamos la sentencia
        $sql = "SELECT tUser.name, tUser.surname, tUser.email, tUser.jobs, tUser.price_per_hour, tUser.latitude, tUser.longitude  FROM tUser WHERE tUser.id = (?)";
        $stmt = $mysqli->prepare($sql);
        $stmt->bind_param("i", $idworker);
        $stmt->execute();
        $resultado = $stmt->get_result();
        // Si no se encuentra a ningún trabajador se devuelve un mensaje
                    if (mysqli_num_rows($resultado) == 0) {
                    ?>
                        <div class="contenedorLogin">
                            <p>No existe un trabajador con ese id, para volver a main pulsa <a href='/main.php'>aquí</a>.</p>
                        </div>
                        <?php
                    } else {
                        ?>
			 <?php
                        // Guardo el resultado de la consulta en una variable $fila para después mostrar los datos por pantalla.
                         $fila = mysqli_fetch_array($resultado);
                        ?>
			<!-- Muestro los datos del trabajador y pongo un enlace para contratarlo -->
			<div class="contenedorWorker">
				<h2>Datos del trabajador</h2>
				<hr>
  				<p><span class="negrita">Nombre:</span> <?php echo $fila[0] ?> </p>
  				<p><span class="negrita">Apellidos:</span> <?php echo $fila[1] ?> </p>
  				<p><span class="negrita">Email:</span> <?php echo $fila[2] ?> </p>
  				<p><span class="negrita">Trabajos:</span> <?php echo $fila[3] ?> </p>
  				<p><span class="negrita">Precio por hora:</span> <?php echo $fila[4] ?> </p>
  				<p><span class="negrita">Localización:</span> <?php echo $fila[5] ?>, <?php echo $fila[6] ?>. </p>
				<br>
			        <a href="/hire.php?worker_id=<?php echo $idworker?>">Contrátame</a>
				<br>
			</div>
                    <?php
                    }
                    ?>
                </tbody>
            </table>
        </div>

    <?php
    }
    $stmt->close();
    mysqli_close($mysqli);
    ?>
</body>

</html>
